#include <linux/init.h>
#include <linux/module.h>
#include <linux/ioport.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <asm/io.h>
#include <linux/slab.h>
#include <asm/uaccess.h>

#include "simple_serial.h"

int major;
int minor;
struct cdev uart;

struct file_operations serial_fops = {
	.owner = THIS_MODULE,
	.read = serial_read,
	.write = serial_write,
	.open = serial_open,
	.release = serial_release,
	.unlocked_ioctl = serial_ioctl
};

void set_baud(int baud)
{

	unsigned char line_cntl = 0;
	line_cntl = inb(LINE_CTRL_REG);
	rmb();

	line_cntl |= DLAB_EN;

	outb(line_cntl, LINE_CTRL_REG);
	wmb();

	if(baud < 256){

		outb(baud, BAUD_LOWER);

		outb(0x00, BAUD_HIGHER);
		wmb();

	} else {

		outb(baud, BAUD_LOWER);
		wmb();
		baud >>= 8;

		outb(baud, BAUD_HIGHER);
		wmb();
	}

	line_cntl &= ~(DLAB_EN);

	outb(line_cntl, LINE_CTRL_REG); //DLAB = 0
	wmb();
}

void set_bits(int bits)
{

	unsigned char line_cntl = 0;
	line_cntl = inb(LINE_CTRL_REG);
	rmb();

	line_cntl &= ~(0x03);
	line_cntl |= bits;

	outb(line_cntl, LINE_CTRL_REG);
	wmb();
}

long serial_ioctl(struct file *filp, unsigned int cmd, 
					unsigned long arg)
{
	int ret = 0;

	if(_IOC_TYPE(cmd) != SERIAL_IOC_MAGIC){
		return -ENOTTY;
	}

	if(_IOC_NR(cmd) > SERIAL_IOC_MAXNR){
		return -ENOTTY;
	}

	switch(cmd){

		case SERIAL_SBAUD:
			set_baud(arg);
			break;

		case SERIAL_SBITS:
			set_bits(arg);
			break;

		default:
			printk(KERN_WARNING "ioctl fail\n");
			break;
	}

	return ret;
}

int serial_open(struct inode *inode, struct file *filp)
{
	set_baud(BAUD9600);
	set_bits(BITS_8);

	printk(KERN_WARNING "There was an attempt\n");

	return 0;
}

int serial_release (struct inode *inode, struct file *filp)
{
	return 0;
}

ssize_t serial_read(struct file *filp, char __user *buf, size_t count,
					loff_t *f_pos)
{
	int ret = 0, i;

	unsigned char *kbuf = kmalloc(count, GFP_KERNEL);

	if(kbuf){

		for(i = 0; i < count; ++i){
			unsigned char line_status = 0;
			rmb();

			while(!(line_status & READ_READY)){
				line_status = inb(LINE_STAT_REG);
				rmb();
			}

			kbuf[i] = inb(DATA_REG);
			rmb();
		}
	} else {
		return -ENOMEM;
	}

	ret = copy_to_user(buf, kbuf, count);

	kfree(kbuf);
	return ret;
}

ssize_t serial_write(struct file *filp, const char __user *buf, 
				  size_t count, loff_t *f_pos)
{
	int i, ret = count;
	unsigned char line_status = 0;

	unsigned char *kbuf = kmalloc(count, GFP_KERNEL);

	if(!kbuf){
		return -ENOMEM;
	}

	if(copy_from_user(kbuf, buf, count)){
		kfree(kbuf);
		return -EFAULT;
	}

	for(i = 0; i < count; ++i){

		line_status = inb(LINE_STAT_REG);

		while(!(line_status & WRITE_READY)){
			line_status = inb(LINE_STAT_REG);
			rmb();
		}

		outb(kbuf[i], DATA_REG);
		wmb();

		--ret;
	}

	kfree(kbuf);
	return ret;
}

static int simple_serial_init(void)
{
	int ret = 0;
	dev_t dev = 0;
	int dev_no;

	if((!request_region(PORT_NUM, NR_OF_REGS ,"simple_serial"))){
		printk(KERN_WARNING "simple_serial can't get port\n");
		return -ENODEV;
	} else {
		printk(KERN_WARNING "successful!\n");
	}

	ret = alloc_chrdev_region(&dev, minor, 1,"simple_serial");
	major = MAJOR(dev);

	if(ret < 0){
		printk(KERN_WARNING "simple_serial can't get major number");
		release_region(PORT_NUM, 1);
		return ret;
	}

	dev_no = MKDEV(major, minor);
	
	cdev_init(&uart, &serial_fops);
	uart.owner = THIS_MODULE;
	uart.ops = &serial_fops;

	ret = cdev_add(&uart, dev_no, 1);

	if(ret < 0){
		printk(KERN_WARNING "error adding device %d\n", ret);
		return ret;
	}

	return 0;
}

static void simple_serial_exit(void)
{

	int dev_no = MKDEV(major, minor);

	release_region(PORT_NUM, NR_OF_REGS);
	unregister_chrdev_region(dev_no, 1);
	printk(KERN_WARNING "removed\n");
}

module_init(simple_serial_init)
module_exit(simple_serial_exit)

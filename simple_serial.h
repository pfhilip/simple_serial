#ifndef SIMPLE_SERIAL_H
#define SIMPLE_SERIAL_H

#include <linux/ioctl.h>

int serial_open(struct inode *inode, struct file *filp);
int serial_release(struct inode *inode, struct file *filp);

ssize_t serial_read(struct file *filp, char __user *buf, size_t count,
					loff_t *f_pos);
ssize_t serial_write(struct file *filp, const char __user *buf, 
				  size_t count, loff_t *f_pos);

static int simple_serial_init(void);
static void simple_serial_exit(void);

long serial_ioctl(struct file *filp, unsigned int cmd, 
					unsigned long arg);

void set_bits(int bits);
void set_baud(int baud);

#define DATA_REG 		0x3F8
#define INT_EN_REG 		0X3F9
#define INT_ID_REG  	0x3FA
#define FIFO_CTRL_REG 	0x3FA
#define LINE_CTRL_REG	0x3FB
#define MODEM_CTRL_REG	0x3FC
#define LINE_STAT_REG	0x3FD
#define MODEM_STAT_REG	0x3FE
#define SCRATCH_REG		0x3FF

#define BAUD_LOWER		0x3F8
#define BAUD_HIGHER		0x3F9

#define BAUD9600		0x0C

#define DLAB_EN			0x80

#define BITS_8			0x03
#define BITS_7			0x02
#define BITS_6			0x01
#define BITS_5			0x00

#define READ_READY		0x01
#define WRITE_READY		0x40

#define PORT_NUM		0x3F8
#define NR_OF_REGS		8



#define SERIAL_IOC_MAGIC	'Z'
	
#define SERIAL_SBAUD		_IOW(SERIAL_IOC_MAGIC, 1, int)
#define SERIAL_SBITS		_IOW(SERIAL_IOC_MAGIC, 2, int)

#define SERIAL_IOC_MAXNR	2

#endif